package com.example.criminalintent

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.R

class CrimeFragment : Fragment() {
    private lateinit var crimeRecyclerView: RecyclerView
    private var adapter: CrimeAdapter? = null
    private lateinit var totalCrimeCountTextView: TextView
    private lateinit var rollbackButton: Button
    private lateinit var saveButton: Button
    private lateinit var newCrimeTitleEditText: EditText


    // 用于存储最近添加的犯罪记录
    private val rollbackStack = mutableListOf<Crime>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_crime_list, container, false)
        crimeRecyclerView = view.findViewById(R.id.crime_recycler_view)
        crimeRecyclerView.layoutManager = LinearLayoutManager(context)
        totalCrimeCountTextView = view.findViewById(R.id.total_crime_count)
        rollbackButton = view.findViewById(R.id.rollback_button)
        saveButton = view.findViewById(R.id.save_button)
        newCrimeTitleEditText = view.findViewById(R.id.new_crime_title)

        rollbackButton.setOnClickListener {
            rollbackLastCrime()
        }

        saveButton.setOnClickListener {
            addNewCrime()
        }

        updateUI()
        return view
    }

    private fun updateUI() {
        val crimeLab = CrimeLab.get(context)
        val crimes = crimeLab.crimes
        adapter = CrimeAdapter(crimes)
        crimeRecyclerView.adapter = adapter
        totalCrimeCountTextView.text = "Total Crimes: ${crimes.size}"
    }

    // 回滚最后添加的犯罪记录
    private fun rollbackLastCrime() {
        if (rollbackStack.isNotEmpty()) {
            val lastCrime = rollbackStack.removeAt(rollbackStack.size - 1)
            val crimeLab = CrimeLab.get(context)
            crimeLab.crimes.remove(lastCrime)
            updateUI()
        }
    }

    // 添加新犯罪记录
    private fun addNewCrime() {
        val title = newCrimeTitleEditText.text.toString()
        if (title.isNotEmpty()) {
            val newCrime = Crime(title = title)
            val crimeLab = CrimeLab.get(context)
            crimeLab.addCrime(newCrime)
            rollbackStack.add(newCrime)
            newCrimeTitleEditText.text.clear()
            updateUI()
        }
    }
}
//11
