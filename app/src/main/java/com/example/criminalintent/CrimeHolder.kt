package com.example.criminalintent

import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.RatingBar
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.myapplication.R

class CrimeHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.list_item_crime, parent, false)) {
    private var titleTextView: TextView? = null
    private var dateTextView: TextView? = null
    private var solvedCheckBox: CheckBox? = null
    private var shareButton: Button? = null
    private var ratingBar: RatingBar? = null
    private var ratingText: TextView? = null

    private var mapImageView: ImageView? = null

    private fun getStaticMapUrl(latitude: Double, longitude: Double): String {
        val apiKey = "4eaf28f3e3f7e8f3b9284cf43868177d"//my own one, dont leak pls
        val url = "https://restapi.amap.com/v3/staticmap?location=$longitude,$latitude&zoom=10&size=750*300&markers=mid,,A:$longitude,$latitude&key=$apiKey"
        Log.d("StaticMapURL", url)
        return url    }

    init {
        titleTextView = itemView.findViewById(R.id.crime_title)
        dateTextView = itemView.findViewById(R.id.crime_date)
        solvedCheckBox = itemView.findViewById(R.id.crime_solved)
        shareButton = itemView.findViewById(R.id.share_button)
        ratingBar = itemView.findViewById(R.id.ratingBar)
        ratingText = itemView.findViewById(R.id.ratingText)

        mapImageView = itemView.findViewById(R.id.crime_map)
    }

    fun bind(crime: Crime) {
        titleTextView?.text = crime.title
        dateTextView?.text = crime.date.toString()
        solvedCheckBox?.isChecked = crime.isSolved




        val mapUrl = getStaticMapUrl(crime.latitude, crime.longitude)
        Log.d("CrimeHolder", "Loading map image from URL: $mapUrl")

        mapImageView?.let {
            Glide.with(itemView.context)
                .load(mapUrl)
                .into(it)
        }

        // 设置分享按钮
        shareButton?.setOnClickListener {
            val shareIntent = Intent(Intent.ACTION_SEND).apply {
                type = "text/plain"
                putExtra(Intent.EXTRA_TEXT, getCrimeReport(crime))
            }
            itemView.context.startActivity(shareIntent)
        }

        // 设置RatingBar和TextView
        ratingBar?.rating = crime.rating
        ratingText?.text = "Rating: ${crime.rating}"

        ratingBar?.setOnRatingBarChangeListener { _, rating, _ ->
            crime.rating = rating
            ratingText?.text = "Rating: $rating"
        }
    }

    private fun getCrimeReport(crime: Crime): String {
        val solvedString = if (crime.isSolved) {
            "Crime is solved"
        } else {
            "Crime is not solved"
        }
        return "${crime.title}\nDate: ${crime.date}\nStatus: $solvedString"
    }
}
