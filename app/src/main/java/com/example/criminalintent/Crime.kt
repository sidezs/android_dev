package com.example.criminalintent

import java.util.Date
import java.util.UUID

data class Crime(
    val id: UUID = UUID.randomUUID(),
    var title: String = "",
    var date: Date = Date(),
    var isSolved: Boolean = false,
    var rating: Float = 0.0f, // 添加rating字段
    var latitude: Double = 0.0,
    var longitude: Double = 0.0
    //11
)

