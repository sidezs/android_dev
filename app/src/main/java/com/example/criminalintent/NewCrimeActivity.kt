package com.example.myapplication

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.criminalintent.Crime
import com.example.myapplication.R
import java.util.Date

class NewCrimeActivity : AppCompatActivity() {

    private lateinit var titleField: EditText
    private lateinit var dateField: EditText
    private lateinit var solvedCheckBox: CheckBox
    private lateinit var saveButton: Button

    @SuppressLint("MissingInflatedId")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_crime)

        titleField = findViewById(R.id.crime_title)
        dateField = findViewById(R.id.crime_date)
        solvedCheckBox = findViewById(R.id.crime_solved)
        saveButton = findViewById(R.id.save_button)

        saveButton.setOnClickListener {
            val title = titleField.text.toString()
            val date = Date()  // You can add a date picker to select the date
            val isSolved = solvedCheckBox.isChecked

            val crime = Crime(title = title, date = date, isSolved = isSolved)

            setResult(Activity.RESULT_OK)
            finish()
        }
    }


    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, NewCrimeActivity::class.java)
        }
    }
}
//11
