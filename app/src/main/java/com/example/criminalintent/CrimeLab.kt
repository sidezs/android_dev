package com.example.criminalintent

import android.content.Context
import java.util.Date

class CrimeLab private constructor(context: Context) {
    val crimes = mutableListOf<Crime>()

    init {
        //mor specific example
        crimes += Crime(
            title = "Crime in Beijing",
            date = Date(),
            isSolved = true,
            rating = 4.5f,
            latitude = 39.9042,
            longitude = 116.4074
        )

        crimes += Crime(
            title = "Crime in Shanghai",
            date = Date(),
            isSolved = false,
            rating = 3.0f,
            latitude = 31.2304,
            longitude = 121.4737
        )

        crimes += Crime(
            title = "Crime in Guangzhou",
            date = Date(),
            isSolved = true,
            rating = 5.0f,
            latitude = 23.1291,
            longitude = 113.2644
        )

        //  other crimes for demonstration
        for (i in 0 until 12) {
            val crime = Crime()
            crime.title = "Crime #$i"
            crime.isSolved = i % 2 == 0
            crimes += crime
        }
    }

    fun addCrime(crime: Crime) {
        crimes += crime
    }

    fun removeCrime(crime: Crime) {
        crimes -= crime
    }
    companion object {
        private var INSTANCE: CrimeLab? = null

        fun get(context: Context?): CrimeLab {
            return INSTANCE ?: CrimeLab(context!!).also { INSTANCE = it }
        }
    }

}
//11
